# -*- coding: utf-8 -*-
#---------------------------------------
##   程序：糗百爬虫&客户端显示
##   版本：0.2
##   作者：tq
##   日期：2014-04-17
##   语言：Python 3.3
##   操作：按左右箭头换页
##   功能：逐条浏览今日糗事
#---------------------------------------

import re
import urllib.request
from urllib.request import Request
import threading
import time
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import ui_wormform
import os
import shutil

imagePattern = re.compile("<img.*?>")

#----------- 处理页面上的各种标签 -----------
class HTML_Tool:
    # 用非 贪婪模式 匹配 \t 或者 \n 或者 空格 或者 超链接 或者 图片
    # .*?对应的是非 贪婪模式，而.*对应的是贪婪模式
    BgnCharToNoneRex = re.compile("(\t|\n| |<a.*?>|<img.*?>)")
    
    # 用非 贪婪模式 匹配 任意<>标签
    EndCharToNoneRex = re.compile("<.*?>")

    # 用非 贪婪模式 匹配 任意<p>标签
    BgnPartRex = re.compile("<p.*?>")
    CharToNewLineRex = re.compile("(<br/>|</p>|<tr>|<div>|</div>)")
    CharToNextTabRex = re.compile("<td>")

    # 将一些html的符号实体转变为原始符号
    replaceTab = [("<","<"),(">",">"),("&","&"),("&","\""),(" "," ")]
    
    def Replace_Char(self,x):
        x = self.BgnCharToNoneRex.sub("",x)
        x = self.BgnPartRex.sub("\n    ",x)
        x = self.CharToNewLineRex.sub("\n",x)
        x = self.CharToNextTabRex.sub("\t",x)
        x = self.EndCharToNoneRex.sub("",x)

        for t in self.replaceTab:
            x = x.replace(t[0],t[1])
        return x
#----------- 处理页面上的各种标签 -----------


#----------- 加载处理糗事百科 -----------
class HTML_Model(QMainWindow):
    #信号定义
    signal_page = pyqtSignal(int,name="page")
    signal_update = pyqtSignal(int,name="page")
    signal_alert = pyqtSignal()
    
    def __init__(self,parent=None):
        super(HTML_Model,self).__init__(parent)
        self.ui = ui_wormform.Ui_qiubai()
        self.ui.setupUi(self)

        #page number for url request
        self.webPage = 1
        #current page for client display
        self.currentPage = 1
        #以糗事为单位的缓冲内容
        self.cachedContent = []
        self.cachedImgFlag = []
        self.cachedContentNum = 0
        self.myTool = HTML_Tool()
        self.enable = False
        
        def updateDisplay(page):
            self.ui.contentDisplayLabel.setText(self.myTool.Replace_Char(self.cachedContent[page][1]))
            
            if self.cachedImgFlag[page]==1:
                self.ui.imgDisplayLabel.show()
                pixmap = QPixmap("./images/%d"%page)
                m = QMatrix()
                x=self.ui.imgDisplayLabel.width()
                y=self.ui.imgDisplayLabel.height()
                m.scale(x,y)
                pixmap_new = pixmap.scaled(x, y, Qt.KeepAspectRatio, Qt.SmoothTransformation)
                self.ui.imgDisplayLabel.setPixmap(pixmap_new)
                self.ui.imgDisplayLabel.setAlignment(Qt.AlignCenter)
            else:
                self.ui.imgDisplayLabel.hide()

        def updatePageNum(page):
            self.ui.pageNumber.setText(str(page))

        def NonLinkAlert():
            QMessageBox.information(None,"Information",'无法链接糗事百科！')
       
        self.signal_update.connect(updateDisplay)
        self.signal_page.connect(updatePageNum)
        self.signal_alert.connect(NonLinkAlert)
        self.ui.preButton.clicked.connect(self.preButtonClicked)
        self.ui.nextButton.clicked.connect(self.nextButtonClicked)
        #判断目录下如果没有images目录，则生成一个
        if os.path.isdir('./images/') is not True:
            os.mkdir('./images/')
        else:
            shutil.rmtree('./images')
            os.mkdir('./images/')
        
        
    # 将所有的段子都扣出来，添加到列表中并且返回列表
    def GetPage(self,newPage):
        myUrl = "http://m.qiushibaike.com/hot/page/" + newPage
        myheaders = {'Accept-Charset':'GBK,utf-8;q=0.7,*;q=0.3','User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.151 Safari/534.16'}

        req = Request(url=myUrl,headers=myheaders)
        myResponse = urllib.request.urlopen(req)
        myPage = myResponse.read()
        #encode的作用是将unicode编码转换成其他编码的字符串
        #decode的作用是将其他编码的字符串转换成unicode编码
        
        unicodePage = myPage.decode("utf-8")
        # 找出所有class="content"的div标记
        #re.S是任意匹配模式，也就是.可以匹配换行符
        #myItems = re.findall('<div.*?class="content".*?title="(.*?)">(.*?)</div>',unicodePage,re.S)
        myItems = re.findall('<div.*?class="content".*?title="(.*?)">(.*?)</div>(.*?)id',unicodePage,re.S)
        for item in myItems:
            # item 中第一个是div的标题，也就是时间
            # item 中第二个是div的内容，也就是内容
            #mutex.acquire()
            self.cachedContent.append([item[0].replace("\n",""),item[1].replace("\n","")])
            if item[2].find('img')!=-1:
                imgItem = re.findall('<img.*?src="(.*?)".*?',item[2],re.S)
                imgUrl = imgItem[0]
                filename="%d.jpeg"%(len(self.cachedContent)-1)
                urllib.request.urlretrieve(imgUrl,"./images/"+filename)
                self.cachedImgFlag.append(1)
            else:
                self.cachedImgFlag.append(0)
            #mutex.release()
            self.cachedContentNum += 1
        #return items

    # 用于加载新的段子
    def LoadPage(self):
        # 如果用户未输入quit则一直运行
        while self.enable:
            # 如果缓冲的页面内容小于2个
            #if len(self.pages) < 2:
            if self.cachedContentNum-self.currentPage < 10:
                try:
                    # 获取新的页面中的段子们
                    #myPage = self.GetPage(str(self.cachedWebPage))
                    self.GetPage(str(self.webPage))
                    self.webPage += 1
                    #self.pages.append(myPage)
                except:
                    self.signal_alert.emit()
            else:
                time.sleep(1)

    def preButtonClicked(self):
        if self.currentPage>1:
            self.currentPage -= 1
            #self.updateDisplay(self.currentPage)
            self.signal_update.emit(self.currentPage-1)
            #self.ui.pageNumber.setText(str(self.currentPage))
            self.signal_page.emit(self.currentPage)
        else:
            QMessageBox.information(None,"Information","Already the 1st page")

    def nextButtonClicked(self):
        if self.currentPage<self.cachedContentNum:
            self.currentPage += 1
            #self.updateDisplay(self.currentPage)
            self.signal_update.emit(self.currentPage-1)
            #self.ui.pageNumber.setText(str(self.currentPage))
            self.signal_page.emit(self.currentPage)
        else:
            QMessageBox.information(None,"Information","Waiting for updating")
        

    
    def Start(self):
        self.enable = True
        #self.ui.contentDisplayLabel.setText(u'正在加载中请稍候......')

        # 新建一个线程在后台加载段子并存储
        #thread.start_new_thread(self.LoadPage,())
        load = threading.Thread(target=self.LoadPage)
        load.start()
       
        #----------- 加载处理糗事百科 -----------
        while self.cachedContentNum<1:
            pass
        self.signal_update.emit(self.currentPage-1)
        self.signal_page.emit(self.currentPage)
        



#----------- 程序的入口处 -----------
print(u"""
---------------------------------------
   程序：糗百爬虫&客户端显示
   版本：0.2
   作者：tq
   日期：2014-04-17
   语言：Python 3.3
   操作：按左右箭头换页
   功能：逐条浏览今日糗事
---------------------------------------
""")


#print(u'请按下回车浏览今日的糗百内容：')
#input(' ')
#mutex = threading.Lock()
app=QApplication(sys.argv)
myModel = HTML_Model()
myModel.show()
myModel.Start()
sys.exit(app.exec_())
