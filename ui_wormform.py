# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'worm_form.ui'
#
# Created: Mon Apr 21 01:31:25 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_qiubai(object):
    def setupUi(self, qiubai):
        qiubai.setObjectName(_fromUtf8("qiubai"))
        qiubai.resize(460, 304)
        qiubai.setMinimumSize(QtCore.QSize(250, 200))
        qiubai.setMaximumSize(QtCore.QSize(600, 400))
        self.centralwidget = QtGui.QWidget(qiubai)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.name = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.name.setFont(font)
        self.name.setObjectName(_fromUtf8("name"))
        self.horizontalLayout.addWidget(self.name)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(5)
        self.horizontalLayout_2.setContentsMargins(30, -1, 30, -1)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.preButton = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.preButton.sizePolicy().hasHeightForWidth())
        self.preButton.setSizePolicy(sizePolicy)
        self.preButton.setMinimumSize(QtCore.QSize(40, 0))
        self.preButton.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("pre.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.preButton.setIcon(icon)
        self.preButton.setObjectName(_fromUtf8("preButton"))
        self.horizontalLayout_2.addWidget(self.preButton)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        self.pageNumber = QtGui.QLineEdit(self.centralwidget)
        self.pageNumber.setMaximumSize(QtCore.QSize(40, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.pageNumber.setFont(font)
        self.pageNumber.setAlignment(QtCore.Qt.AlignCenter)
        self.pageNumber.setObjectName(_fromUtf8("pageNumber"))
        self.horizontalLayout_2.addWidget(self.pageNumber)
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.nextButton = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nextButton.sizePolicy().hasHeightForWidth())
        self.nextButton.setSizePolicy(sizePolicy)
        self.nextButton.setMinimumSize(QtCore.QSize(40, 0))
        self.nextButton.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("next.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.nextButton.setIcon(icon1)
        self.nextButton.setObjectName(_fromUtf8("nextButton"))
        self.horizontalLayout_2.addWidget(self.nextButton)
        self.gridLayout.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)
        self.contentLayout = QtGui.QHBoxLayout()
        self.contentLayout.setObjectName(_fromUtf8("contentLayout"))
        self.contentDisplayLabel = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.contentDisplayLabel.setFont(font)
        self.contentDisplayLabel.setCursor(QtGui.QCursor(QtCore.Qt.IBeamCursor))
        self.contentDisplayLabel.setFrameShape(QtGui.QFrame.Box)
        self.contentDisplayLabel.setText(_fromUtf8(""))
        self.contentDisplayLabel.setWordWrap(True)
        self.contentDisplayLabel.setMargin(2)
        self.contentDisplayLabel.setObjectName(_fromUtf8("contentDisplayLabel"))
        self.contentLayout.addWidget(self.contentDisplayLabel)
        self.imgDisplayLabel = QtGui.QLabel(self.centralwidget)
        self.imgDisplayLabel.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.imgDisplayLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.imgDisplayLabel.setText(_fromUtf8(""))
        self.imgDisplayLabel.setMargin(2)
        self.imgDisplayLabel.setObjectName(_fromUtf8("imgDisplayLabel"))
        self.contentLayout.addWidget(self.imgDisplayLabel)
        self.contentLayout.setStretch(0, 2)
        self.contentLayout.setStretch(1, 3)
        self.gridLayout.addLayout(self.contentLayout, 2, 0, 1, 1)
        self.gridLayout.setRowStretch(1, 1)
        self.gridLayout.setRowStretch(2, 6)
        self.gridLayout.setRowStretch(3, 2)
        qiubai.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(qiubai)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 460, 23))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        qiubai.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(qiubai)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        qiubai.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(qiubai)
        QtCore.QMetaObject.connectSlotsByName(qiubai)

    def retranslateUi(self, qiubai):
        qiubai.setWindowTitle(_translate("qiubai", "MainWindow", None))
        self.name.setText(_translate("qiubai", "--段子--", None))
        self.label.setText(_translate("qiubai", "第", None))
        self.label_2.setText(_translate("qiubai", "页", None))
        self.menuFile.setTitle(_translate("qiubai", "File", None))

