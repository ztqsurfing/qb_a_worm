# Project intro #
* intro: this is a brief implementation of a crawler for "www.qiushibaike.com", and a local window is designed using pyqt to present the fetched items
* version: this is version 3, while ver 1 isn't thread safe and ver 2 is designed for xm with python2.7

# How to run #
* dependencies: python 3.3 and pyqt 4.10
* two way:1) open the worm.py file with python IDLE, and press F5 to compile and run; Alternatively, 2) you can enter the './bin' dir and double click the executable file to get the same result

# Any advice? #
* feel free to contact - zhoutongqing1991@gmail.com